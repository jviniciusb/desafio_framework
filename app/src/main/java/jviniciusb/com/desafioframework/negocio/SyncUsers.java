package jviniciusb.com.desafioframework.negocio;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Collections;

import jviniciusb.com.desafioframework.dados.AsyncHttpConnection;
import jviniciusb.com.desafioframework.dados.Sessao;

/*Classe para sincronização de lista de usuários disponíveis*/
public class SyncUsers implements AsyncHttpConnection.AsyncHttpConnectionInterface {

    public static final String URL_USUARIOS = "https://jsonplaceholder.typicode.com/users";

    private SyncInterface callback;

    public SyncUsers(SyncInterface callback) {

        this.callback = callback;
    }

    public void execute() {
        new AsyncHttpConnection(this,
                new ArrayList<String>(Collections.singletonList(URL_USUARIOS))).execute();
    }

    @Override
    public void onResponse(ArrayList<String> respostas) {

        if(respostas.size() > 0) {

            String strJson = respostas.get(0);
            JsonArray jsonUsuarios = (JsonArray) new JsonParser().parse(strJson);

            if(jsonUsuarios != null
                    && jsonUsuarios.size() > 0) {

                Sessao.setUsuarios(jsonUsuarios);
                callback.onSuccess();
            }
            else {
                callback.onFailure();
            }
        }
        else {
            callback.onFailure();
        }
    }

    @Override
    public void onWithoutConnection() {
        callback.onWithouConnection();
    }
}
