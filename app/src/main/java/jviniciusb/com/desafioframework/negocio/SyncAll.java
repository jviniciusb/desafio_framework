package jviniciusb.com.desafioframework.negocio;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;

import jviniciusb.com.desafioframework.dados.AsyncHttpConnection;
import jviniciusb.com.desafioframework.dados.Persistencia;

/*Classe para sincronização de todos os dados necessários para o funcionamento do aplicativo*/
public class SyncAll implements AsyncHttpConnection.AsyncHttpConnectionInterface {

    /*Urls de acesso para os dados em jsonplaceholder*/
    private static final String URL_TODOS = "https://jsonplaceholder.typicode.com/todos";
    private static final String URL_ALBUMS = "https://jsonplaceholder.typicode.com/albums";
    private static final String URL_POSTS = "https://jsonplaceholder.typicode.com/posts";
    private static final String URL_COMMENTS = "https://jsonplaceholder.typicode.com/comments";
    private static final String URL_PHOTOS = "https://jsonplaceholder.typicode.com/photos";

    private SyncInterface callback;

    /*Contrutor recebe o callbak para repasse de eventos*/
    public SyncAll(SyncInterface callback) {
        this.callback = callback;
    }

    /*Inicia sincronização dos dados utilizando a classe de conexão*/
    public void execute() {

        ArrayList<String> urls = new ArrayList<>(Arrays.asList(
                URL_ALBUMS+ "?userId=" + Persistencia.getUserId(),
                URL_POSTS + "?userId="  + Persistencia.getUserId(),
                URL_TODOS + "?userId="  + Persistencia.getUserId(),
                URL_COMMENTS,
                URL_PHOTOS));

        new AsyncHttpConnection(this, urls).execute();
    }

    @Override
    /*Metodo executado como callback para o evento de resposta da classe de conexão.
    * Tratamento da resposta*/
    public void onResponse(ArrayList<String> resposta) {

        JsonArray jsonAlbuns, jsonTodos, jsonPostagens, jsonComentarios, jsonFotos;

        JsonParser parser = new JsonParser();

        jsonAlbuns = (JsonArray) parser.parse(resposta.get(0));
        jsonPostagens = (JsonArray) parser.parse(resposta.get(1));
        jsonTodos = (JsonArray) parser.parse(resposta.get(2));
        jsonComentarios = (JsonArray) parser.parse(resposta.get(3));
        jsonFotos = (JsonArray) parser.parse(resposta.get(4));

        if(jsonAlbuns != null && jsonAlbuns.size() > 0
                && jsonPostagens != null && jsonPostagens.size() > 0
                && jsonTodos != null && jsonTodos.size() > 0) {

            Persistencia.setAlbuns(jsonAlbuns);
            Persistencia.setPostagens(jsonPostagens);
            Persistencia.setToDos(jsonTodos);
            Persistencia.setComentarios(jsonComentarios);
            Persistencia.setFotos(jsonFotos);

            callback.onSuccess();
        }
        else {
            callback.onFailure();
        }
    }

    @Override
    /*Metodo executado como callback para o evento de "sem conexão" da classe de conexão*/
    public void onWithoutConnection() {
        /**/
        callback.onWithouConnection();
    }
}
