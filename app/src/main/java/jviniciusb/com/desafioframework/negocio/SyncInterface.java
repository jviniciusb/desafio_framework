package jviniciusb.com.desafioframework.negocio;

/*Interface utilizada como callback para eventos de sincronização de dados*/
public interface SyncInterface {

    /*Executado quando a sincronização é bem sucedida*/
    void onSuccess();

    /*Executado quando a sincronização falha*/
    void onFailure();

    /*Executado quando a sincronização falha por falta de conexão à Internet*/
    void onWithouConnection();
}
