package jviniciusb.com.desafioframework.dados;

import com.google.gson.annotations.SerializedName;

/*Objetos da classe são isntanciados via Gson*/
public class Photo {

    @SerializedName("albumId")
    private long albumId;
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String url;
    @SerializedName("thumbnailUrl")
    private String thumbnailUrl;

    /*Retorna o id do álbum ao qual a foto pertence*/
    public long getAlbumId() {
        return albumId;
    }

    /*Retorna o título da foto*/
    public String getTitle() {
        return title;
    }

    /*Retorna a url para acesso a foto*/
    public String getUrl() {
        return url;
    }

    /*Retorna url para acesso à miniatura da foto*/
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }
}
