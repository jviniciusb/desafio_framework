package jviniciusb.com.desafioframework.dados;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/*Classe para manipulação e acesso dos dados enquanto app está aberto*/
public class Sessao {

    /*Dados para utilização enquanto aplicativo estiver aberto*/
    private static ArrayList<Todo> todosList = new ArrayList<>();
    private static ArrayList<User> usersList= new ArrayList<>();
    private static ArrayList<Post> postsList = new ArrayList<>();
    private static ArrayList<Comment> commentsList = new ArrayList<>();
    private static ArrayList<Album> albumsList = new ArrayList<>();
    private static ArrayList<Photo> photosList = new ArrayList<>();

    /*Limpa os dados de sessão*/
    public static void limpaDados() {
        todosList.clear();
        usersList.clear();
        postsList.clear();
        commentsList.clear();
        albumsList.clear();
        postsList.clear();
    }

    /*----------------------------------------------------------------------------------------------
     * Gerência dos todos
     *---------------------------------------------------------------------------------------------*/

    public static ArrayList<Todo> getTodos() {
        if(Sessao.todosList.size() == 0) {
            carregaTodos();
        }
        return todosList;
    }

    public static void salvaTodos(ArrayList<Todo> todos) {
        Sessao.todosList = todos;
        String strJson = new Gson().toJson(todos);
        JsonArray jsonTodos = (JsonArray) new JsonParser().parse(strJson);
        Persistencia.setToDos(jsonTodos);
    }

    private static void carregaTodos() {

        Type listType = new TypeToken<ArrayList<Todo>>() {}.getType();
        Sessao.todosList = new Gson().fromJson(Persistencia.getToDos(), listType);
    }

    /*----------------------------------------------------------------------------------------------
     * Gerência dos usuarios
     *---------------------------------------------------------------------------------------------*/

    public static ArrayList<User> getUsuarios() {

        return usersList;
    }

    public static User getUsuario(int i) {
        return usersList.get(i);
    }

    public static void setUsuarios(JsonArray jsonUsuarios) {
        Type listType = new TypeToken<ArrayList<User>>() {}.getType();
        Sessao.usersList = new Gson().fromJson(jsonUsuarios, listType);
    }

    /*----------------------------------------------------------------------------------------------
     * Gerência dos posts
     *---------------------------------------------------------------------------------------------*/

    public static ArrayList<Post> getPostsList() {
        if(postsList.size() == 0) {
            carregaPosts();
        }
        return Sessao.postsList;
    }

    private static void carregaPosts() {
        Type listType = new TypeToken<ArrayList<Post>>() {}.getType();
        Sessao.postsList = new Gson().fromJson(Persistencia.getPostagens(), listType);
    }

    /*----------------------------------------------------------------------------------------------
     * Gerência dos comentarios
     *---------------------------------------------------------------------------------------------*/

    private static ArrayList<Comment> getCommentsList() {
        if(commentsList.size() == 0) {
            carregaComments();
        }
        return Sessao.commentsList;
    }

    public static ArrayList<Comment> getCommentsPostId(long postId) {
        ArrayList<Comment> commentsPostId = new ArrayList<>();
        ArrayList<Comment> allComments = getCommentsList();

        for(Comment comment : allComments) {

            if(comment.getPostId() == postId) {
                commentsPostId.add(comment);
            }
        }

        return commentsPostId;
    }

    private static void carregaComments() {
        Type listType = new TypeToken<ArrayList<Comment>>() {}.getType();
        Sessao.commentsList = new Gson().fromJson(Persistencia.getComentarios(), listType);
    }

    /*----------------------------------------------------------------------------------------------
     * Gerência dos albuns
     *---------------------------------------------------------------------------------------------*/

    public static ArrayList<Album> getAlbumsList() {
        if(albumsList.size() == 0) {
            carregaAlbums();
        }
        return Sessao.albumsList;
    }

    private static void carregaAlbums() {
        Type listType = new TypeToken<ArrayList<Album>>() {}.getType();
        Sessao.albumsList = new Gson().fromJson(Persistencia.getAlbuns(), listType);
    }

    /*----------------------------------------------------------------------------------------------
     * Gerência de fotos
     *---------------------------------------------------------------------------------------------*/

    private static ArrayList<Photo> getPhotosList() {
        if(photosList.size() == 0) {
            carregaFotos();
        }
        return Sessao.photosList;
    }

    public static Photo getFirstPhoto(long albumId) {

        return getPhotosAlbumId(albumId).get(0);
    }

    public static ArrayList<Photo> getPhotosAlbumId(long albumId) {
        ArrayList<Photo> photosAlbumId = new ArrayList<>();
        ArrayList<Photo> allPhotos = getPhotosList();

        for(Photo photo : allPhotos) {

            if(photo.getAlbumId() == albumId) {
                photosAlbumId.add(photo);
            }
        }

        return photosAlbumId;
    }

    private static void carregaFotos() {
        Type listType = new TypeToken<ArrayList<Photo>>() {}.getType();
        Sessao.photosList = new Gson().fromJson(Persistencia.getFotos(), listType);
    }
}
