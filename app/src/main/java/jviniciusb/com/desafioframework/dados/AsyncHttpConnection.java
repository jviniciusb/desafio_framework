package jviniciusb.com.desafioframework.dados;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/*Classe para acesso web de forma assíncrona*/
public class AsyncHttpConnection extends AsyncTask<Void, Void, ArrayList<String>> {

    // Tipos de métodos de envio  http
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String PUT = "PUT";
    // Timeout para SocketTimeoutException
    private static final int timeout = 10000;
    // Guarda estado de conectividade com a Internet
    private static boolean offline = false;

    private AsyncHttpConnectionInterface callback;
    private String metodo;
    private String paramentros;
    private ArrayList<String> urls;

    /*Versao completa do construtor*/
    public AsyncHttpConnection(AsyncHttpConnectionInterface callback, String metodo, ArrayList<String> urls, String paramentros) {

        this.callback = callback;
        this.metodo = metodo;
        this.paramentros = paramentros;
        this.urls = urls;
    }

    /*Versao simplificada do construtor para acesso via GET*/
    public AsyncHttpConnection(AsyncHttpConnectionInterface conexaoInterface, ArrayList<String> urls) {

        this(conexaoInterface, GET, urls, "");
    }

    @Override
    protected ArrayList<String> doInBackground(Void... voids) {
        /*Executa acesso web em background*/
        if(urls.size() == 0) {
            return new ArrayList<String>();
        }
        else {
            return execute(metodo, urls, paramentros);
        }
    }

    @Override
    protected void onPostExecute(ArrayList<String> respostas) {
        if (offline) {
            // Caso tenha detectado falta de acesso à internet
            callback.onWithoutConnection();
        }
        else {
            // Caso tenha acesso a Internet
            callback.onResponse(respostas);
        }
    }

    private ArrayList<String> execute(String method, ArrayList<String> urls, String params) {

        /*Se estava offline verifica se a rede voltou*/
        if (offline) {
            verificaRede();
            /*Se a rede nao houver retornado retorna vazio*/
            if (offline) {
                return new ArrayList<>();
            }
        }

        /*Faz a requisição para o webservice*/
        ArrayList<String> respostas = web(urls, params, method);
        if (respostas.size() == 0) {
            /*Se a requisição retornou vazio verifica se tem Internet*/
            verificaRede();
        }

        return respostas;
    }

    /*Executa acessos web */
    private static ArrayList<String> web(ArrayList<String> urls, String parametros, String metodo) {

        ArrayList<String> respostas = new ArrayList<>();

        try {
            for(String url_str : urls) {

                StringBuilder response = new StringBuilder();
                URL url = new URL(url_str);

                // Abre conexão http
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setConnectTimeout(timeout);
                httpURLConnection.setReadTimeout(timeout);

                httpURLConnection.setRequestMethod(metodo);
                if (metodo.equals(POST)) {
                    httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    httpURLConnection.setDoOutput(true);
                    DataOutputStream dStream = new DataOutputStream(httpURLConnection.getOutputStream());
                    dStream.writeBytes(parametros);
                    dStream.flush();
                    dStream.close();
                }

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response.append(line);
                }

                httpURLConnection.disconnect();
                respostas.add(response.toString());
            }

            return respostas;
        }
        catch (IOException e) {
            return new ArrayList<>();
        }
    }

    /*Verifica se existe conexão com a internet*/
    private static void verificaRede() {

        try {
            URL url = new URL("https://www.google.com.br");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("HEAD");
            urlConnection.setConnectTimeout(5000);
            urlConnection.setReadTimeout(5000);

            int responseCode = urlConnection.getResponseCode();
            offline = (responseCode != HttpURLConnection.HTTP_OK);
        } catch (IOException e) {
            offline = true;
        }
    }

    public interface AsyncHttpConnectionInterface {

        void onResponse(ArrayList<String> respostas);

        void onWithoutConnection();
    }
}
