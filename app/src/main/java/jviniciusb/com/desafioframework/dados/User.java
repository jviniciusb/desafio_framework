package jviniciusb.com.desafioframework.dados;

import com.google.gson.annotations.SerializedName;

/*Objetos da classe são isntanciados via Gson*/
public class User {

    @SerializedName("username")
    private String username;
    @SerializedName("id")
    private long id;

    User(){};

    /*Retorna o nome do usuário*/
    public String getUsername() {
        return username;
    }

    /*Retorna o id do usuário*/
    public long getId() {
        return id;
    }
}
