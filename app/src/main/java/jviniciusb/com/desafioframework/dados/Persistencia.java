package jviniciusb.com.desafioframework.dados;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

/*Classe para persistência dos dados utilizando sharedPreferences*/
public class Persistencia extends Application {

    private static final String SHARED_PREF_KEY = "JVINICIUSB_KEY";
    private static SharedPreferences sharedPref;
    private static SharedPreferences.Editor sharedPrefEditor;

    @Override
    /*Método é chamdado sempre que alguma instância do aplicativo
    (activity inicial, serviços, broadcastReceivers) é iniciada*/
    public void onCreate() {
        super.onCreate();

        sharedPref = getApplicationContext().getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
    }

    /*Limpa todos os dados armazenados*/
    public static void limpaDados() {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.clear();
        sharedPrefEditor.apply();
    }

    /*----------------------------------------------------------------------------------------------
    * Armazenamento do identificador de usuario
    *---------------------------------------------------------------------------------------------*/
    private final static String USER_ID_KEY = "USER_ID_KEY";

    /*Retorna o identificador de usuario*/
    public static String getUserId() {
        return sharedPref.getString(USER_ID_KEY, "");
    }

    /*Salva o identificador de usuario*/
    public static void setUserId(String userId) {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putString(USER_ID_KEY, userId);
        sharedPrefEditor.apply();
    }

    /*----------------------------------------------------------------------------------------------
     * Armazenamento do nome de usuario
     *---------------------------------------------------------------------------------------------*/
    private final static String USER_NAME_KEY = "USER_NAME_KEY";

    /*Retorna o identificador de usuario*/
    public static String getNomeUsuario() {
        return sharedPref.getString(USER_NAME_KEY, "");
    }

    /*Salva o JSON sicronizado de TO-DOs*/
    public static void setNomeUsuario(String userName) {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putString(USER_NAME_KEY, userName);
        sharedPrefEditor.apply();
    }

    /*----------------------------------------------------------------------------------------------
     * Armazenamento do JSON de TO-DOs
     *---------------------------------------------------------------------------------------------*/
    private final static String TODOS_KEY = "TODOS_KEY";

    /*Retorna o JSON salvo de TO-DOs*/
    public static JsonArray getToDos() {
        String strJson = sharedPref.getString(TODOS_KEY, new Gson().toJson("[]"));

        return  (JsonArray) new JsonParser().parse(strJson);
    }

    /*Salva o JSON sicronizado de TO-DOs*/
    public static void setToDos(JsonArray jsonTodos) {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putString(TODOS_KEY, new Gson().toJson(jsonTodos));
        sharedPrefEditor.apply();
    }

    /*----------------------------------------------------------------------------------------------
     * Armazenamento do JSON de postagens
     *---------------------------------------------------------------------------------------------*/
    private final static String POSTAGENS_KEY = "POSTAGENS_KEY";

    /*Retorna o JSON salvo de TO-DOs*/
    public static JsonArray getPostagens() {
        String strJson = sharedPref.getString(POSTAGENS_KEY, new Gson().toJson("[]"));

        return  (JsonArray) new JsonParser().parse(strJson);
    }

    /*Salva o JSON sicronizado de TO-DOs*/
    public static void setPostagens(JsonArray jsonPostagens) {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putString(POSTAGENS_KEY, new Gson().toJson(jsonPostagens));
        sharedPrefEditor.apply();
    }

    /*----------------------------------------------------------------------------------------------
     * Armazenamento do JSON de albuns
     *---------------------------------------------------------------------------------------------*/
    private final static String ALBUNS_KEY = "ALBUNS_KEY";

    /*Retorna o JSON salvo de TO-DOs*/
    public static JsonArray getAlbuns() {
        String strJson = sharedPref.getString(ALBUNS_KEY, new Gson().toJson("[]"));

        return  (JsonArray) new JsonParser().parse(strJson);
    }

    /*Salva o JSON sicronizado de TO-DOs*/
    public static void setAlbuns(JsonArray jsonAlbuns) {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putString(ALBUNS_KEY, jsonAlbuns.toString());
        sharedPrefEditor.apply();
    }

    /*----------------------------------------------------------------------------------------------
     * Armazenamento do JSON de comentarios
     *---------------------------------------------------------------------------------------------*/
    private final static String COMMENTS_KEY = "COMMENTS_KEY";

    /*Retorna o JSON salvo de TO-DOs*/
    public static JsonArray getComentarios() {
        String strJson = sharedPref.getString(COMMENTS_KEY, new Gson().toJson("[]"));

        return  (JsonArray) new JsonParser().parse(strJson);
    }

    /*Salva o JSON sicronizado de TO-DOs*/
    public static void setComentarios(JsonArray jsonComentarios) {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putString(COMMENTS_KEY, new Gson().toJson(jsonComentarios));
        sharedPrefEditor.apply();
    }

    /*----------------------------------------------------------------------------------------------
     * Armazenamento do JSON de comentarios
     *---------------------------------------------------------------------------------------------*/
    private final static String PHOTOS_KEY = "PHOTOS_KEY";

    /*Retorna o JSON salvo de TO-DOs*/
    public static JsonArray getFotos() {
        String strJson = sharedPref.getString(PHOTOS_KEY, new Gson().toJson("[]"));

        return (JsonArray) new JsonParser().parse(strJson);
    }

    /*Salva o JSON sicronizado de TO-DOs*/
    public static void setFotos(JsonArray jsonFotos) {
        sharedPrefEditor = sharedPref.edit();
        sharedPrefEditor.putString(PHOTOS_KEY, new Gson().toJson(jsonFotos));
        sharedPrefEditor.apply();
    }
}
