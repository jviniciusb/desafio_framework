package jviniciusb.com.desafioframework.dados;

import com.google.gson.annotations.SerializedName;

/*Objetos da classe são isntanciados via Gson*/
public class Todo {

    @SerializedName("completed")
    private boolean completed;
    @SerializedName("title")
    private String title;

    /*Retorna o títudo da tarefa*/
    public String getTitle() {
        return title;
    }

    /*Retorna o estado da tarefa, completada(true) ou pendente(false)*/
    public boolean getCompleted() {
        return completed;
    }

    /*Altera o estado da tarefa para completada(true) ou pendente(false)*/
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
