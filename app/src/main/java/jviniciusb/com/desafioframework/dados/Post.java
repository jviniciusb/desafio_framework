package jviniciusb.com.desafioframework.dados;

import com.google.gson.annotations.SerializedName;

/*Objetos da classe são isntanciados via Gson*/
public class Post {

    @SerializedName("title")
    private String title;
    @SerializedName("body")
    private String body;
    @SerializedName("id")
    private long id;

    /*Retorna o título da postagem*/
    public String getTitle() {
        return title;
    }

    /*Retorna o conteúdo da postagem*/
    public String getBody() {
        return body;
    }

    /*Retorna o id da postagem*/
    public long getId() {
        return id;
    }
}
