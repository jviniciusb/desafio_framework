package jviniciusb.com.desafioframework.dados;

import com.google.gson.annotations.SerializedName;

/*Objetos da classe são isntanciados via Gson*/
public class Comment {

    @SerializedName("postId")
    private long postId;
    @SerializedName("name")
    private String name;
    @SerializedName("email")
    private String email;
    @SerializedName("body")
    private String body;

    /*Retorna o id de um comentário*/
    public long getPostId() {
        return postId;
    }

    /*Rertorna o nome de um comentário*/
    public String getName() {
        return name;
    }

    /*Retorna o email do autor do comentário*/
    public String getEmail() {
        return email;
    }

    /*Retorna o conteúdo do comentário*/
    public String getBody() {
        return body;
    }
}
