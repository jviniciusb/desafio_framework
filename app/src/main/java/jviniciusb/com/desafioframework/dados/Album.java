package jviniciusb.com.desafioframework.dados;

import com.google.gson.annotations.SerializedName;

/*Objetos da classe são isntanciados via Gson*/
public class Album {

    @SerializedName("title")
    private String title;

    @SerializedName("id")
    private long id;

    /*Rertorna o título de album*/
    public String getTitle() {
        return title;
    }

    /*Rertonra o id de um album*/
    public long getId() {
        return id;
    }
}
