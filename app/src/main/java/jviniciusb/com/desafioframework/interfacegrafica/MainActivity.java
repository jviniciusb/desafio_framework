package jviniciusb.com.desafioframework.interfacegrafica;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import jviniciusb.com.desafioframework.R;
import jviniciusb.com.desafioframework.dados.Persistencia;
import jviniciusb.com.desafioframework.dados.Sessao;
import jviniciusb.com.desafioframework.negocio.SyncAll;
import jviniciusb.com.desafioframework.negocio.SyncInterface;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private int opcao = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.menu_albuns, R.string.menu_albuns);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        /*Configura o click dos botões e menuda tela pricipal*/
        ((NavigationView) findViewById(R.id.nvMenuPrincipal)).setNavigationItemSelectedListener(this);
        findViewById(R.id.bt_postagens).setOnClickListener(this);
        findViewById(R.id.bt_albuns).setOnClickListener(this);
        findViewById(R.id.bt_todos).setOnClickListener(this);

        View header = ((NavigationView) findViewById(R.id.nvMenuPrincipal)).getHeaderView(0);
        // Preenche as iformações do header do navigation view
        ((TextView) header.findViewById(R.id.tv_nome_usuario)).setText(Persistencia.getNomeUsuario());
    }

    @Override
    /*Captura evento de clique do botão voltar do Android*/
    public void onBackPressed() {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    /*Metodo chamando quando um item do Drawer menu é clicado*/
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        /*Delega a função de clique para o metodo onClick*/
        View view = new View(this);
        view.setId(item.getItemId());
        onClick(view);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    /*Recebe os eventos de click e delega a função para o metodo correspondente*/
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.menu_albuns
                || id == R.id.bt_albuns) {

            opcao = ListasActivity.ALBUM_OPTION;
            iniciaTela(ListasActivity.class);
        }
        else if (id == R.id.menu_postagens
                || id == R.id.bt_postagens) {

            opcao = ListasActivity.POST_OPTION;
            iniciaTela(ListasActivity.class);
        }
        else if (id == R.id.menu_todos
                || id == R.id.bt_todos) {

            opcao = ListasActivity.TODO_OPTION;
            iniciaTela(ListasActivity.class);
        }
        else if(id == R.id.menu_user) {
            mudaUsuario();
        }
        else if(id == R.id.menu_ressincroniza) {
            ressincronizaDados();
        }
        else if(id == R.id.menu_sobre) {
            iniciaTela(SobreActivity.class);
        }
    }

    /*Inicia uma nova tela de acordo com a activity recebida*/
    private void iniciaTela(Class activity) {

        Intent intent = new Intent(this, activity);
        intent.putExtra(ListasActivity.OPTION, opcao);
        startActivity(intent);
    }

    /*Limpa os dados da aplicação e retorna para a tela de escolha de usuário*/
    private void mudaUsuario() {

        Sessao.limpaDados();
        Persistencia.limpaDados();
        iniciaTela(SplashActivity.class);
        finish();
    }

    private void ressincronizaDados() {

        Sessao.limpaDados();
        new SyncAll(new SyncInterface() {
            @Override
            public void onSuccess() {

                MyDialogs.cancelDialog();
                MyDialogs.showSnackbar(MainActivity.this,
                        findViewById(R.id.main_content),
                        R.string.texto_dados_redefinidos);
            }

            @Override
            public void onFailure() {

                MyDialogs.cancelDialog();
                MyDialogs.showSnackbar(MainActivity.this,
                        findViewById(R.id.main_content),
                        R.string.texto_falha_redefinicao);
            }

            @Override
            public void onWithouConnection() {

                MyDialogs.cancelDialog();
                MyDialogs.showSnackbar(MainActivity.this,
                        findViewById(R.id.main_content),
                        R.string.texto_falha_conexao);
            }
        }).execute();

        MyDialogs.showDialog(this, R.string.texto_redefinindo_dados);
    }
}
