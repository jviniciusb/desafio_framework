package jviniciusb.com.desafioframework.interfacegrafica;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import jviniciusb.com.desafioframework.R;

/*Classe para exibição de alertas e avisos padronizados*/
public class MyDialogs {

    private static AlertDialog dialog;
    /*Exibe um dialogo de espera*/
    public static void showDialog(Activity activity, int mensagem) {

        @SuppressLint("InflateParams")
        View viewInflated = activity.getLayoutInflater().inflate(R.layout.layout_progress, null, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false)
                .setMessage(mensagem)
                .setView(viewInflated);
        dialog = builder.create();

        if(!activity.isFinishing()) {
            dialog.show();
        }
    }

    /*Fecha dialogo de espera*/
    public static void cancelDialog() {
        if(dialog != null
                && dialog.isShowing()) {

            dialog.dismiss();
        }
    }

    /*Exibe um alerta para o usuário*/
    public static void showAlert(final Activity activity, int mensagem, final boolean finish) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(mensagem)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(finish) {
                            ActivityCompat.finishAffinity(activity);
                            System.exit(0);
                        }
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();

        if(!activity.isFinishing()) {
            alert.show();
        }
    }

    /*Exibe uma aviso para o usuário*/
    public static void showSnackbar(Activity activity, View parent, int mensagem) {

        Snackbar snackbar = Snackbar.make(parent, mensagem, Snackbar.LENGTH_LONG);
        snackbar.setAction(android.R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Apenas fecha o snackbar*/
            }
        });
        TextView text = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        text.setMaxLines(3);

        if(!activity.isFinishing()) {
            snackbar.show();
        }
    }
}
