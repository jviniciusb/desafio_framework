package jviniciusb.com.desafioframework.interfacegrafica;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import jviniciusb.com.desafioframework.R;
import jviniciusb.com.desafioframework.dados.Sessao;
import jviniciusb.com.desafioframework.dados.Todo;

@SuppressWarnings("unchecked")
public class AdapterTodos
        extends RecyclerView.Adapter<AdapterTodos.TodoViewHolder>
        implements View.OnClickListener {

    private ArrayList<Todo> todos;
    private LayoutInflater layoutInflater;
    private Context context;

    AdapterTodos(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.todos = Sessao.getTodos();
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemTodo = layoutInflater.inflate(R.layout.item_todo, parent, false);
        return new TodoViewHolder(itemTodo);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {

        Todo todo = todos.get(position);

        holder.tvTituloTodo.setText(todo.getTitle());

        boolean bool = todo.getCompleted();
        holder.cbTodo.setChecked(bool);
        holder.cbTodo.setClickable(false);
        holder.cbTodo.setTag(position);

        /*Configura evento de clique para mudança de status das tarefas*/
        holder.itemView.setOnClickListener(this);

        /*Troca a cor de fundo do item da lista de acordo com o status da tarefa*/
        Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(ContextCompat.getDrawable(context,
                R.drawable.shape_background_rounded)));
        int corBackground;
        if(bool) {
            corBackground = ContextCompat.getColor(context, R.color.colorPrimaryLight);
        }
        else {
            corBackground = ContextCompat.getColor(context, R.color.colorAccent);
        }
        DrawableCompat.setTint(wrappedDrawable, corBackground);
        holder.itemView.setBackground(wrappedDrawable);
    }

    @Override
    public int getItemCount() {
        return todos.size();
    }

    @Override
    /*Altera o status da tarefa para completada ou pendente de acordo com o status atual*/
    public void onClick(View view) {

        CheckBox cbTodo = view.findViewById(R.id.cb_todo);
        cbTodo.setChecked(!cbTodo.isChecked());

        int position = (int) cbTodo.getTag();
        Todo todo = todos.get(position);
        todo.setCompleted(cbTodo.isChecked());
        todos.remove(position);
        todos.add(position, todo);
        notifyItemChanged(position);
        Sessao.salvaTodos(todos);
    }

    class TodoViewHolder extends RecyclerView.ViewHolder{

        private CheckBox cbTodo;
        private TextView tvTituloTodo;

        TodoViewHolder(View itemView) {
            super(itemView);

            cbTodo = itemView.findViewById(R.id.cb_todo);
            tvTituloTodo = itemView.findViewById(R.id.tv_titulo_todo);
        }
    }
}
