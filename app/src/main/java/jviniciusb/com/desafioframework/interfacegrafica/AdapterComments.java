package jviniciusb.com.desafioframework.interfacegrafica;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import jviniciusb.com.desafioframework.R;
import jviniciusb.com.desafioframework.dados.Comment;
import jviniciusb.com.desafioframework.dados.Sessao;

@SuppressWarnings("unchecked")
public class AdapterComments
        extends RecyclerView.Adapter<AdapterComments.CommentsViewHolder> {

    private ArrayList<Comment> commentsList;
    private LayoutInflater layoutInflater;
    private Context context;

    AdapterComments(Context context, long postId) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.commentsList = Sessao.getCommentsPostId(postId);
    }


    @NonNull
    @Override
    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemComment = layoutInflater.inflate(R.layout.item_comment, parent, false);
        return new CommentsViewHolder(itemComment);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsViewHolder holder, int position) {

        Comment comment = commentsList.get(position);
        holder.tvTituloComment.setText(comment.getEmail());
        holder.tvCorpoComment.setText(comment.getBody());

        Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(ContextCompat.getDrawable(context,
                R.drawable.shape_background_rounded)));
        int corBackground = ContextCompat.getColor(context, R.color.colorPrimaryLight);

        DrawableCompat.setTint(wrappedDrawable, corBackground);
        holder.itemView.setBackground(wrappedDrawable);
    }

    @Override
    public int getItemCount() {
        return commentsList.size();
    }


    class CommentsViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTituloComment;
        private TextView tvCorpoComment;

        CommentsViewHolder(View itemView) {
            super(itemView);

            tvTituloComment = itemView.findViewById(R.id.tv_titulo_comment);
            tvCorpoComment = itemView.findViewById(R.id.tv_corpo_comment);
        }
    }
}
