package jviniciusb.com.desafioframework.interfacegrafica;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import jviniciusb.com.desafioframework.R;
import jviniciusb.com.desafioframework.dados.Photo;
import jviniciusb.com.desafioframework.dados.Sessao;

@SuppressWarnings("unchecked")
public class AdapterPhotos
        extends RecyclerView.Adapter<AdapterPhotos.PhotoViewHolder> {

    private static final int LOAD_SIZE = 6;
    private  ArrayList<Photo> photosListAll;
    private  ArrayList<Photo> photosListCurrent = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private Context context;

    AdapterPhotos(Context context, long albumId) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.photosListAll = Sessao.getPhotosAlbumId(albumId);

        RecyclerView rcPhotos = ((Activity) context).findViewById(R.id.rc_photos);

        /*Configura evento para a rolagem de intens.
        Permite carregar mais itens dinamicamente.*/
        rcPhotos.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                GridLayoutManager glm = (GridLayoutManager) recyclerView.getLayoutManager();

                /*Carrega mais imagens sempre que chega ao final da lista e ainda existem
                * imagens disponíveis*/
                if(photosListCurrent.size() == glm.findLastCompletelyVisibleItemPosition() + 1
                        && photosListCurrent.size() < photosListAll.size()) {

                    loadMore();
                }
            }
        });
        loadMore();
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemPhoto = layoutInflater.inflate(R.layout.item_photo, parent, false);
        return new PhotoViewHolder(itemPhoto);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder holder, int position) {

        Photo photo = photosListCurrent.get(position);

        holder.tvTituloPhoto.setText(photo.getTitle());

        /*Carrega as imagens utilizando a biblioteca picasso*/
        Picasso.get().load(photo.getUrl())
                .placeholder(R.drawable.ic_photo_blue_96dp)
                .error(R.drawable.ic_photo_blue_96dp)
                .into(holder.ivPhoto);

        Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(ContextCompat.getDrawable(context,
                R.drawable.shape_background_rounded)));
        int corBackground = ContextCompat.getColor(context, R.color.colorPrimaryLight);

        DrawableCompat.setTint(wrappedDrawable, corBackground);
        holder.itemView.setBackground(wrappedDrawable);
    }

    @Override
    public int getItemCount() {
        return photosListCurrent.size();
    }


    class PhotoViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTituloPhoto;
        private ImageView ivPhoto;

        PhotoViewHolder(View itemView) {
            super(itemView);

            tvTituloPhoto = itemView.findViewById(R.id.tv_titulo_photo);
            ivPhoto = itemView.findViewById(R.id.iv_photo);
        }
    }

    /*Carrega mais imagens para serem exibidas.
    * Evita que todas as imagens tenham de ser carregadas de uma única vez*/
    private void loadMore (){
        int i = photosListCurrent.size();
        int count = 0;
        while (i < photosListAll.size()
                && count < LOAD_SIZE) {

            photosListCurrent.add(photosListAll.get(i));
            notifyItemInserted(i);
            i++;
            count++;
        }
    }
}
