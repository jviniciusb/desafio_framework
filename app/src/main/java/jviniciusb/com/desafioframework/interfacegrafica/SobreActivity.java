package jviniciusb.com.desafioframework.interfacegrafica;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import jviniciusb.com.desafioframework.BuildConfig;
import jviniciusb.com.desafioframework.R;

public class SobreActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        mostraVersao();
        findViewById(R.id.tv_contato).setOnClickListener(this);
    }

    /*Mostra a vesão atual do aplicativo*/
    private void mostraVersao() {
        TextView tvVesao = findViewById(R.id.tv_versao);
        String aux = tvVesao.getText() + " " + BuildConfig.VERSION_NAME;
        tvVesao.setText(aux);
    }

    @Override
    /*Acessa aplicativo de email para contato*/
    public void onClick(View view) {

        String email = "mailto:" + ((TextView) view).getText().toString();

        Intent emailItent  = new Intent(Intent.ACTION_SENDTO, Uri.parse(email));
        Intent chooser = Intent.createChooser(emailItent, getString(R.string.texto_escolha_aplicativo));
        chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(chooser);
    }
}
