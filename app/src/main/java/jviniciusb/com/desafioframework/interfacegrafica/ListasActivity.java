package jviniciusb.com.desafioframework.interfacegrafica;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import jviniciusb.com.desafioframework.R;

public class ListasActivity extends AppCompatActivity {

    public static final String OPTION = "OPTION";
    public static final int POST_OPTION = 0;
    public static final int ALBUM_OPTION = 1;
    public static final int TODO_OPTION = 2;

    private boolean backMainActivity = true;

    static View.OnClickListener onClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        carregaLista();
        criaListener();
    }

    @Override
    /*Sobrescreve envento de clique do botão voltar do android para controle de sequência de telas*/
    public void onBackPressed() {

        if(backMainActivity) {
            super.onBackPressed();
        }
        else {
            carregaLista();
            backMainActivity = true;
        }
    }

    private void carregaLista() {
        setContentView(R.layout.activity_listas);

        RecyclerView rcTodos = findViewById(R.id.rc_lista);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rcTodos.setLayoutManager(llm);

        TextView tvTituloLista = findViewById(R.id.tv_titulo_lista);

        int opcao = getIntent().getIntExtra(OPTION, -1);
        /*Carrega os elementos de lista de acordo com a opção selecionada na tela principal*/
        if(opcao == POST_OPTION) {
            tvTituloLista.setText(R.string.texto_titulo_lista_post);
            rcTodos.setAdapter(new AdapterPosts(this));
        }
        else if(opcao == ALBUM_OPTION) {
            tvTituloLista.setText(R.string.texto_titulo_lista_album);
            rcTodos.setAdapter(new AdapterAlbums(this));

        }
        else if(opcao == TODO_OPTION) {
            tvTituloLista.setText(R.string.texto_titulo_lista_todo);
            rcTodos.setAdapter(new AdapterTodos(this));
        }
    }

    /*Cria um listener para o evento de clique em um album*/
    private void criaListener() {

        onClickListener = new View.OnClickListener() {
            @Override
            /*Carrega as fotos do album selecionado*/
            public void onClick(View view) {

                backMainActivity = false;
                setContentView(R.layout.layout_photos);

                RecyclerView rcPhotos = findViewById(R.id.rc_photos);
                GridLayoutManager glm = new GridLayoutManager(ListasActivity.this, 3);
                glm.setOrientation(LinearLayoutManager.VERTICAL);
                rcPhotos.setLayoutManager(glm);
                rcPhotos.setHasFixedSize(true);

                int position = (int) view.getTag();
                rcPhotos.setAdapter(new AdapterPhotos(ListasActivity.this, AdapterAlbums.getAlbumId(position)));
            }
        };
    }
}