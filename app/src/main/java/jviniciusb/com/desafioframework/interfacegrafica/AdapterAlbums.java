package jviniciusb.com.desafioframework.interfacegrafica;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import jviniciusb.com.desafioframework.R;
import jviniciusb.com.desafioframework.dados.Album;
import jviniciusb.com.desafioframework.dados.Photo;
import jviniciusb.com.desafioframework.dados.Sessao;

@SuppressWarnings("unchecked")
public class AdapterAlbums
        extends RecyclerView.Adapter<AdapterAlbums.AlbumViewHolder> {

    private static ArrayList<Album> albumsList;
    private LayoutInflater layoutInflater;
    private Context context;

    AdapterAlbums(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        AdapterAlbums.albumsList = Sessao.getAlbumsList();
    }

    @NonNull
    @Override
    public AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemAlbum = layoutInflater.inflate(R.layout.item_album, parent, false);
        return new AlbumViewHolder(itemAlbum);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumViewHolder holder, int position) {

        Album album = albumsList.get(position);

        /*Utiliza a biblioteca picasso para carregar as imagens*/
        Photo firstPhoto = Sessao.getFirstPhoto(album.getId());
        Picasso.get().load(firstPhoto.getUrl())
                .placeholder(R.drawable.ic_photo_96dp)
                .error(R.drawable.ic_photo_96dp)
                .into(holder.ivAlbum);

        holder.tvTituloAlbum.setText(album.getTitle());

        /*Configura evento de clique no album para acessar as fotos*/
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(ListasActivity.onClickListener);

        Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(ContextCompat.getDrawable(context,
                R.drawable.shape_background_rounded)));
        int corBackground = ContextCompat.getColor(context, R.color.colorAccent);

        DrawableCompat.setTint(wrappedDrawable, corBackground);
        holder.itemView.setBackground(wrappedDrawable);
    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTituloAlbum;
        private ImageView ivAlbum;

        AlbumViewHolder(View itemView) {
            super(itemView);

            tvTituloAlbum = itemView.findViewById(R.id.tv_titulo_album);
            ivAlbum = itemView.findViewById(R.id.iv_album);
        }
    }

    /*Retorna o id do album selecionado*/
    public static long getAlbumId(int position) {
        Album album = albumsList.get(position);
        return album.getId();
    }
}
