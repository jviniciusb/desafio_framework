package jviniciusb.com.desafioframework.interfacegrafica;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import jviniciusb.com.desafioframework.R;
import jviniciusb.com.desafioframework.dados.Post;
import jviniciusb.com.desafioframework.dados.Sessao;

@SuppressWarnings("unchecked")
public class AdapterPosts
        extends RecyclerView.Adapter<AdapterPosts.PostViewHolder>
        implements View.OnClickListener {

    private ArrayList<Post> postsList;
    private LayoutInflater layoutInflater;
    private Context context;
    private boolean[] showComments;

    AdapterPosts(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.postsList = Sessao.getPostsList();
        showComments = new boolean[postsList.size()];
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemPost = layoutInflater.inflate(R.layout.item_post, parent, false);
        return new PostViewHolder(itemPost);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {

        Post post = postsList.get(position);

        holder.tvTituloPost.setText(post.getTitle());
        holder.tvCorpoPost.setText(post.getBody());
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(this);

        Drawable wrappedDrawable = DrawableCompat.wrap(Objects.requireNonNull(ContextCompat.getDrawable(context,
                R.drawable.shape_background_rounded)));
        int corBackground = ContextCompat.getColor(context, R.color.colorAccent);
        DrawableCompat.setTint(wrappedDrawable, corBackground);
        holder.itemView.setBackground(wrappedDrawable);

        /*Exibe ou esconde os comentarios de um post de acordo com o evento de clique*/
        RecyclerView rc_comments = holder.itemView.findViewById(R.id.rc_comments);
        if(showComments[position]) {
            rc_comments.setVisibility(View.VISIBLE);
            LinearLayoutManager llm = new LinearLayoutManager(context);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            rc_comments.setLayoutManager(llm);
            rc_comments.setAdapter(new AdapterComments(context, post.getId()));
        }
        else {
            rc_comments.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    @Override
    public void onClick(View view) {

        int position = (int) view.getTag();
        showComments[position] = !showComments[position];
        notifyItemChanged(position);
    }

    class PostViewHolder extends RecyclerView.ViewHolder{

        private TextView tvTituloPost;
        private TextView tvCorpoPost;

        PostViewHolder(View itemView) {
            super(itemView);

            tvTituloPost = itemView.findViewById(R.id.tv_titulo_post);
            tvCorpoPost = itemView.findViewById(R.id.tv_corpo_post);
        }
    }
}
