package jviniciusb.com.desafioframework.interfacegrafica;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import jviniciusb.com.desafioframework.R;
import jviniciusb.com.desafioframework.dados.Persistencia;
import jviniciusb.com.desafioframework.dados.Sessao;
import jviniciusb.com.desafioframework.dados.User;
import jviniciusb.com.desafioframework.negocio.SyncAll;
import jviniciusb.com.desafioframework.negocio.SyncInterface;
import jviniciusb.com.desafioframework.negocio.SyncUsers;

public class SplashActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    Handler handlerDelay;
    Runnable run;
    static final int delay = 1500;
    private Spinner spUsuario;
    private String nomeUsuario, idUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        atrasaInicialisacao();
    }

    /*Coloca um delay na exibição da splash screen*/
    private void atrasaInicialisacao() {

        run = new Runnable(){

            public void run() {

                /*Se ja existirem dados sincronizados inicia a tela principal.
                * Caso contrário busca a lista de usuário disoníveis e exibe tela para
                * escolha de usuário*/
                if(!Persistencia.getUserId().equals("")) {
                    iniciMainActivity();
                }
                else {
                    sincronizaUsuarios();
                }
            }
        };

        // A proxima tela somente será iniciada após o tempo de delay
        handlerDelay = new Handler();
        handlerDelay.postDelayed(run, delay);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Evita que a thread continue executando caso o usuario use o botao voltar do android antes de se passar o tempo de delay
        if(run != null) {
            handlerDelay.removeCallbacks(run);
        }
    }

    /*Busca a lista de usuários disponíveis*/
    private void sincronizaUsuarios() {

        new SyncUsers(new SyncInterface() {
            @Override
            public void onSuccess() {

                trocaLayout();
            }

            @Override
            public void onFailure() {
                MyDialogs.showAlert(SplashActivity.this, R.string.texto_falha_dados, true);
            }

            @Override
            public void onWithouConnection() {
                MyDialogs.showAlert(SplashActivity.this, R.string.texto_falha_conexao, true);
            }
        }).execute();
    }

    /*Inicia a tela principal*/
    private void iniciMainActivity() {
        // Configura a execução da próxima tela
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /*Altera layout para escolha do usuário a ser utilizado*/
    private void trocaLayout() {
        setContentView(R.layout.activity_escolhe_usuario);

        spUsuario = findViewById(R.id.sp_usuario);
        spUsuario.setOnItemSelectedListener(this);

        String[] nomes = getNomesUsuarios();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,
                nomes){

            @Override
            public boolean isEnabled(int position){

                /*Desabilita primeira posição do spinner*/
                return !(position == 0);
            }
        };

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUsuario.setAdapter(adapter);

        findViewById(R.id.bt_entrar).setOnClickListener(this);
    }

    /*Inicia a sincronização de dados do usuário selecionado*/
    private void sincronizaDados() {

        Persistencia.setNomeUsuario(nomeUsuario);
        Persistencia.setUserId(idUsuario);

        new SyncAll(new SyncInterface() {
            @Override
            public void onSuccess() {

                MyDialogs.cancelDialog();
                iniciMainActivity();
            }

            @Override
            public void onFailure() {

                MyDialogs.cancelDialog();
                MyDialogs.showAlert(SplashActivity.this, R.string.texto_falha_dados, false);
            }

            @Override
            public void onWithouConnection() {

                MyDialogs.cancelDialog();
                MyDialogs.showAlert(SplashActivity.this, R.string.texto_falha_conexao, false);
            }
        }).execute();

        MyDialogs.showDialog(this, R.string.texto_buscando_dados);
    }

    /*Retorna uma lista de usuarios para escolha no spinner*/
    private String[] getNomesUsuarios() {

        ArrayList<User> usuarios = Sessao.getUsuarios();
        String[] nomesUsuarios = new String[usuarios.size() + 1];
        nomesUsuarios[0] = "Usuários:";
        for(int i = 0; i < usuarios.size(); i++) {
            nomesUsuarios[i + 1] = usuarios.get(i).getUsername();
        }

        return nomesUsuarios;
    }

    @Override
    /*Captura informações do usuário selecionado no spinner*/
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(i > 0) {
            User usuario = Sessao.getUsuario(i - 1);
            idUsuario = usuario.getId() + "";
            nomeUsuario = usuario.getUsername();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        /*Não utilizado*/
    }

    @Override
    /*Inicia sincronização dos dados do usuário selecionado*/
    public void onClick(View view) {
        if(spUsuario.getSelectedItemPosition() == 0) {
            Toast.makeText(this, R.string.texto_nao_escolheu_usuario, Toast.LENGTH_SHORT).show();
        }
        else {
            sincronizaDados();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        /*Não utilizado*/
    }
}
